class MoviesController < ApplicationController
    def index
        @movies = Movie.all
        @ratings = Movie.all_ratings
        @selected_ratings = {"G" => "on"}
        @sorted_by = ""
    end
end
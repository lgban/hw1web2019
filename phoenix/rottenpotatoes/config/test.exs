use Mix.Config

# Configure your database
config :rottenpotatoes, Rottenpotatoes.Repo,
  username: "postgres",
  password: "postgres",
  database: "hw1phoenix",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :rottenpotatoes, RottenpotatoesWeb.Endpoint,
  http: [port: 4002],
  server: true

# Print only warnings and errors during test
config :logger, level: :warn
